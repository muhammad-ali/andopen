1:

GIL is basically a mechanism to control the flow of multiple processes utilizing a shared resource.
For example, lets say, two or more people live in a house where there is only one kitchen.
If two or more people try to cook different dishes at the same time using the same utensils,
there will be a chaos and the outcome might be unexpected or undesired.
In this case, a scheduler would come in handy which would ensure every person gets too cook according to their turn,
and more importantly, while one person is cooking, there are no interruptions from anyone else.

2:

Lets assume we want to fetch some data from an external API. It is a good to fetch that data in a separate server.
In this case we can say we are using a background job. It is a server that run in background separate from our main
server.Our main server is one on which our main application is running. If we use the same main server to fetch data
from some external API then we are giving extra work to our main server. This means the our main server will could be
overwhelmed with so many task. In order to reduce the the load from main server we can use a background job fetch data
for us.
