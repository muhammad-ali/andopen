Rails.application.routes.draw do
  get 'users/profile'

  devise_for :users, :controllers => { registrations: 'users/registrations' }
  authenticated :user do
    root :to => "users#profile", as: :user_profile
  end

  root :to => 'welcome#index'

  get 'users(:format)' => 'users#index', as: :users_list
  get 'user_requests(:format)' => 'users#requests', as: :requests
  get 'friends(:format)' => 'users#friends', as: :friends
  post 'users/:id/add_friend(:format)' => 'users#add_friend', as: :add_friend
  post 'users/:id/accept_request(:format)' => 'users#accept_request', as: :accept_request
  post 'users/:id/remove_friend(:format)' => 'users#remove_friend', as: :remove_friend
end
