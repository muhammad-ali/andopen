class ShortenUrlWorker
  include Sidekiq::Worker

  def perform(user_id)
    client = Bitly::API::Client.new(token: "03650ace4582705df2d18dbd28c2bba5b9e4090e")
    user = User.find(user_id)
    bitlink = client.shorten(long_url: user.url)
    user.short_url = bitlink.link
    user.save
  end
end
