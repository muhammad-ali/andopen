class AddHeadingsWorker
  include Sidekiq::Worker

  require 'nokogiri'
  require 'open-uri'

  def perform(user_id)
    user = User.find(user_id)
    doc = Nokogiri::HTML(URI.open(user.url))
    doc.css('h1').each do |h|
      Heading.create(user_id: user_id, content: h.content)
    end
    doc.css('h2').each do |h|
      Heading.create(user_id: user_id, content: h.content)
    end
    doc.css('h3').each do |h|
      Heading.create(user_id: user_id, content: h.content)
    end
  end
end
