class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :headings
  has_friendship

  validates :name, presence: true
  validates :url, presence: true

  after_create :set_short_url
  after_create :add_headings

  def set_short_url
    ShortenUrlWorker.perform_async(self.id)
  end

  def add_headings
    AddHeadingsWorker.perform_async(self.id)
  end
end
