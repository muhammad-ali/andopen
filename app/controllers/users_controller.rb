class UsersController < ApplicationController
  def profile
    @user = User.includes(:headings).find(current_user.id)
  end

  def index
    @current_user = current_user
    if params[:search].present?
      @users = User.where.not(id: current_user.id).where('name LIKE :search', search: "%#{params[:search]}%")
    else
      @users = User.where.not(id: current_user.id)
    end
  end

  def add_friend
    user = User.find_by(id: params[:id])
    current_user.friend_request(user)
    flash[:success] = "Request sent"
    redirect_to users_list_path
  end

  def requests
    @current_user = current_user
    @users = current_user.requested_friends
  end

  def accept_request
    user = User.find_by(id: params[:id])
    current_user.accept_request(user)
    flash[:success] = "Request accepted"
    redirect_to :back
  end

  def friends
    @users = current_user.friends
  end

  def remove_friend
    user = User.find_by(id: params[:id])
    current_user.remove_friend(user)
    flash[:success] = "Friend removed :("
    redirect_to :back
  end
end
