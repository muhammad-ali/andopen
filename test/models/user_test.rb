require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test 'invalid without user name' do
    u = User.new(email: 'a@b.com', url: "www.google.com")
    refute u.valid?
    assert_not_nil u.errors[:name]
  end

  test 'invalid user without url' do
    u = User.new(email: "ali@g.com", name: "ali mehroz")
    refute u.valid?
    assert_not_nil u.errors[:url]
  end
end
