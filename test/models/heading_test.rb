require 'test_helper'

class HeadingTest < ActiveSupport::TestCase

  test 'invalid without user_id s' do
    h = Heading.new(content: 'Data Analytics')
    refute h.valid?
    assert_not_nil h.errors[:user_id]
  end

  test 'invalid without content' do
    user = users(:one)
    h = Heading.new(user_id: user.id)
    refute h.valid?
    assert_not_nil h.errors[:content]
  end

  test 'valid Heading' do
    user = users(:one)
    h = Heading.new(content: 'Data Analytics', user_id: user.id)
    assert h.valid?
  end
end
