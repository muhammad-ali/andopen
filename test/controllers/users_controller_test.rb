require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  include Warden::Test::Helpers
  include Devise::Test::ControllerHelpers
  # include Devise::Test::IntegrationHelpers

  setup do
    @request.env['HTTP_REFERER'] = 'http://localhost:3000/users'
    @user = users(:one)
    sign_in @user
  end

  test "should get profile" do
    get :profile
    assert_response :success
  end

  test "should add friends" do
    user1 = users(:two)
    post :add_friend, id: user1.id
    user1.accept_request(@user)
    assert_response :redirect
    assert_equal @user.friends.count, 1
    assert_equal user1.friends.count, 1
  end

  test "should remove friends" do
    user1 = users(:two)
    @user.friend_request(user1)
    user1.accept_request(@user)
    assert_equal @user.friends.count, 1
    assert_equal user1.friends.count, 1
    post :remove_friend, id: user1.id
    assert_response :redirect
    assert_equal @user.friends.count, 0
    assert_equal user1.friends.count, 0
  end

  test "friends list" do
    user1 = users(:two)
    @user.friend_request(user1)
    user1.accept_request(@user)
    assert_equal @user.friends.count, 1
    assert_equal user1.friends.count, 1
    get :friends
    assert_response :success
  end

  test "requests list" do
    user1 = users(:two)
    @user.friend_request(user1)
    assert_equal @user.friends.count, 0
    assert_equal user1.friends.count, 0
    get :requests
    assert_response :success
  end

end
